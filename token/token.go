package token

import "strings"

type Token struct {
	Type    string
	Literal string
}

const (
	// Delimiters.
	LPAREN = "("
	RPAREN = ")"

	LBRACE = "{"
	RBRACE = "}"

	LSQUARE = "["
	RSQUARE = "]"

	// Primitive data.
	INT = "INT"

	// Quoting.
	QUOTE = "'"

	// Strings.
	DBLQUOTE = "\""
	STRING = "STRING"

	// Keywords
	KEYWORD = "KEYWORD"

	// Identifiers.
	IDENT = "IDENTIFIER"

	// Skippable stuff
	COMMENT = ";"

	UNKNOWN = "unknown"
	EOF     = "EOF"
)

var Terminators = strings.Join([]string{RPAREN, RBRACE, RSQUARE}, "")
var Beginners = strings.Join([]string{LPAREN, LBRACE, LSQUARE}, "")
var OtherSkippable = QUOTE
var Delimiters = strings.Join([]string{Beginners, Terminators, OtherSkippable}, "")
