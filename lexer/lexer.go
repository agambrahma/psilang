package lexer

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"

	tok "bitbucket.org/agambrahma/PsiLang/token"
)

// Wraps around a byte stream and returns a token stream.
type Lexer struct {
	scanner *bufio.Scanner
}

// Public methods.
func NewLexer(input string) *Lexer {
	l := &Lexer{scanner: bufio.NewScanner(strings.NewReader(input))}
	l.scanner.Split(sexpSplitter)

	return l
}

func (l *Lexer) NextToken() tok.Token {
	if !l.scanner.Scan() {
		return tok.Token{tok.EOF, ""}
	}

	txt := l.scanner.Text()

	// Can we have zero-sized tokens?
	if len(txt) == 0 {
		return tok.Token{tok.UNKNOWN, ""}
	}

	// First, single character tokens.
	if len(txt) == 1 {
		switch txt {
		case "(":
			return tok.Token{tok.LPAREN, txt}
		case ")":
			return tok.Token{tok.RPAREN, txt}
		case "[":
			return tok.Token{tok.LSQUARE, txt}
		case "]":
			return tok.Token{tok.RSQUARE, txt}
		case "{":
			return tok.Token{tok.LBRACE, txt}
		case "}":
			return tok.Token{tok.RBRACE, txt}
		case "'":
			return tok.Token{tok.QUOTE, txt}
		}
	}

	// Based on the first character of the token, we can differentiate
	// numerical values and keywords (assuming a rule that all identifiers
	// start with [a-zA-Z]).
	if txt[0] == ':' {
		return tok.Token{tok.KEYWORD, txt}
	}

	if txt[0] >= '0' && txt[0] <= '9' {
		return tok.Token{tok.INT, txt}
	}

	// Separate out strings based on the presence of a double-quote at the beginning.
	if strings.HasPrefix(txt, tok.DBLQUOTE) {
		// Strings MUST end with a double-quote too!
		if !strings.HasSuffix(txt, tok.DBLQUOTE) {
			panic(fmt.Sprintf(
				"Saw a string without closing delimiter; this shouldn't happen!: %s\n", txt))
		}

		// Return the string without the double-quots
		return tok.Token{tok.STRING, txt[1:len(txt)-1]}
	}

	// Whatever is left, must be an identifier :-)
	return tok.Token{tok.IDENT, txt}
}

// Private helpers.
func sexpSplitter(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// Skip newlines [x]
	// Return open/close parens individually [x]
	// Return open/close square braces individually [x]
	// Return open/close curly braces individually [x]
	// If a double quote is seen, return the rest of the string [x]
	// If a single quote is seen (i.e. not within a string), return it individually [x]
	// Gobble up whitespace [x]
	// Comments are not returned [x]
	// Otherwise, return the next 'word' (should be a keyword, symbol, or number) [x]

	advance, token, err = bufio.ScanRunes(data, atEOF)
	// fmt.Println("Debug: read: [", string(token), "], ", advance)

	// Cumulative advance due to any skipped tokens.
	netAdvance := 0

	skipNewLines := func() {
		for bytes.Equal(token, []byte("\n")) && err == nil {
			netAdvance += advance
			data = data[1:]
			advance, token, err = bufio.ScanRunes(data, atEOF)
		}
	}

	// Skip newlines and keep track of how much we skipped.
	skipNewLines()
	if err != nil {
		return
	}

	// If we didn't really read anything, we're done.
	if len(token) == 0 {
		return
	}
	firstChar := []byte{token[0]}

	// Skip the entirety of any line preceded by a comment (re-scan up to a newline).
	for bytes.Equal(firstChar, []byte(";")) {
		for !bytes.Equal(token, []byte("\n")) && err == nil {
			netAdvance += advance
			data = data[1:]
			advance, token, err = bufio.ScanRunes(data, atEOF)
		}

		skipNewLines()
		if err != nil {
			return
		}
		firstChar = []byte{token[0]}
	}

	// Handle delimiters and other skippables.
	for _, delim := range tok.Delimiters {
		if string(token) == string(delim) {
			// if bytes.Equal(token, []byte("(")) || bytes.Equal(token, []byte(")")) ||
			// 	bytes.Equal(token, []byte("[")) || bytes.Equal(token, []byte("]")) ||
			// 	bytes.Equal(token, []byte("{")) || bytes.Equal(token, []byte("}")) {
			return netAdvance + 1, token, nil
		}
	}

	// FIXME(agam): Implement the rest!

	// Fallthrough: get the next word (but only up to the next delimiter!)
	termPos := bytes.IndexAny(data, tok.Terminators)
	if termPos > 0 {
		advance, token, err = bufio.ScanWords(data[0:termPos], atEOF)
	} else {
		advance, token, err = bufio.ScanWords(data, atEOF)
	}
	return netAdvance + advance, token, err
}
