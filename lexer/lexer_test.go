package lexer

import (
	"testing"

	tok "bitbucket.org/agambrahma/PsiLang/token"
)

func TestSimple(t *testing.T) {
	l := NewLexer(`
(def a "foo")
(def b 123)
; Don't see this line!
;Nor this line!
(def c [a b {:baz 456}])
( whitespace    should    not    matter    )
'(hopefully unevaled)`)

	// FIXME(agam): Write out expectations for the individual tokens present here.

	expectedTokens := []tok.Token{
		{tok.LPAREN, "("},
		{tok.IDENT, "def"},
		{tok.IDENT, "a"},
		{tok.STRING, "foo"},
		{tok.RPAREN, ")"},
		{tok.LPAREN, "("},
		{tok.IDENT, "def"},
		{tok.IDENT, "b"},
		{tok.INT, "123"},
		{tok.RPAREN, ")"},
		{tok.LPAREN, "("},
		{tok.IDENT, "def"},
		{tok.IDENT, "c"},
		{tok.LSQUARE, "["},
		{tok.IDENT, "a"},
		{tok.IDENT, "b"},
		{tok.LBRACE, "{"},
		{tok.KEYWORD, ":baz"},
		{tok.INT, "456"},
		{tok.RBRACE, "}"},
		{tok.RSQUARE, "]"},
		{tok.RPAREN, ")"},
		{tok.LPAREN, "("},
		{tok.IDENT, "whitespace"},
		{tok.IDENT, "should"},
		{tok.IDENT, "not"},
		{tok.IDENT, "matter"},
		{tok.RPAREN, ")"},
		{tok.QUOTE, "'"},
		{tok.LPAREN, "("},
		{tok.IDENT, "hopefully"},
		{tok.IDENT, "unevaled"},
		{tok.RPAREN, ")"},
	}

	t.Logf("Running %d token tests", len(expectedTokens))
	for token, ind := l.NextToken(), 0; token.Type != tok.EOF; token, ind = l.NextToken(), ind+1 {
		if token != expectedTokens[ind] {
			t.Fatalf("Expected token %v(%d), got %v", expectedTokens[ind], ind, token)
		}
	}
}
