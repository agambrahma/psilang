package parser

import (
	"strconv"

	"bitbucket.org/agambrahma/PsiLang/ast"
	"bitbucket.org/agambrahma/PsiLang/lexer"
	tok "bitbucket.org/agambrahma/PsiLang/token"
)

type Parser struct {
	lex *lexer.Lexer
}

func NewParser(input string) *Parser {
	p := &Parser{lex: lexer.NewLexer(input)}
	return p
}

func (p *Parser) NextForm() ast.Node {
	t := p.lex.NextToken()

	// If we've reached EOF, just return nils.
	if t.Type == tok.EOF {
		return ast.NilVal
	}

	// Return singleton nodes if we find singleton elements.
	if t.Type == tok.STRING {
		return &ast.StringNode{Value: t.Literal}
	}

	if t.Type == tok.INT {
		// FIXME(agam): Add more num types
		val, _ := strconv.Atoi(t.Literal)

		return &ast.NumNode{Value: val}
	}

	// Fallthrough
	return ast.NilVal
}
