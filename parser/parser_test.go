package parser

import (
	"testing"

	"bitbucket.org/agambrahma/PsiLang/ast"
)

func TestSingletons(t *testing.T) {
	input := `
"foo"
123
`
	expectedNodes := []ast.Node{
		&ast.StringNode{Value: "foo"},
		&ast.NumNode{Value: 123},
	}

	p := NewParser(input)

	for f, idx := p.NextForm(), 0; f != ast.NilVal; f, idx = p.NextForm(), idx+1 {
		if !f.Equals(expectedNodes[idx]) {
			t.Fatalf("Expected node %v (%d), got %v", expectedNodes[idx], idx, f)
		}
	}
}
