package ast

import "fmt"

type Node interface {
	// FIXME(agam): change to take in a buffer stream
	Show()
	Equals(Node) bool
}

type CommonNode struct {
	// Common stuff for all parsed nodes.
}

// Special 'nil' type :-P
type NilNode struct {
	CommonNode
}

// Only one value of this type
var NilVal = &NilNode{}

func (n *NilNode) Show() {
	fmt.Print("nil")
}

func (n *NilNode) Equals(o Node) bool {
	if o == NilVal && n == NilVal {
		return true
	} else {
		return false
	}
}

// Numbers.
type NumNode struct {
	CommonNode
	Value int
}

func (n *NumNode) Show() {
	fmt.Print("%d", n.Value)
}

func (n *NumNode) Equals(o Node) bool {
	return n.Value == o.(*NumNode).Value
}

// Strings.
type StringNode struct {
	CommonNode
	Value string
}

func (n *StringNode) Show() {
	fmt.Print("%s", n.Value)
}

func (n *StringNode) Equals(o Node) bool {
	return n.Value == o.(*StringNode).Value
}

// Lists.
type ListNode struct {
	CommonNode
	elems []*Node
}
